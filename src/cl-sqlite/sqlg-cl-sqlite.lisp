(in-package :sqlg)
;;;
;;; sqlg-cl-sqlite.lisp: cl-sqlite-specific sqlg code
;;;
;;; https://github.com/dmitryvk/cl-sqlite
;;; http://common-lisp.net/project/cl-sqlite/
;;;
;;; quicklisp calls this system 'sqlite':
;;; (ql:system-apropos "sqlite")
;;; #<SYSTEM sqlite / cl-sqlite-20130615-git / quicklisp 2017-08-30>
;;;

(defparameter *sqlg-lisp-front-end* :cl-sqlite)

;; note: 2000 millisecond timeout may, ultimately, be an issue if many clients are accessing the database
(defparameter *sqlite-timeout* 2000)


#|
If specified, use database connection DB. Otherwise, use default database connection specified by *DB*.
|#

(defun add-rows* (columns values table &key (db *db*))
  (sqlite:with-open-database (database (db-host db))
    (sqlite:with-transaction database
      (dolist (row values) (add-row* table columns row)))))

;; this is per-front-end since cl-sqlite handles things differently and column values
;; must be returned in a wrapper
(defun columns-where (table columns criterion &key (db *db*) unique-p)
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string criterion table)
	   (cons columns))
  (let ((raw-rows (columns-from-table-where-core table columns criterion :db db :unique-p unique-p)))
    (values
     ;; kludge since cl-sqlite seems to take NULL values and return "NIL"
     (mapcar #'(lambda (row)
		 (substitute "" "NIL" row :test #'equalp))
	     raw-rows)
     columns)))

;; see sqlg-postmodern code if need to revise
;;(defun add-rows-chunked (columns values table &key connection)
;;  (add-rows columns values table :connection connection))

;; FIXME: what on earth is this intended to do? where is actual connection established?
(defun database-connect (&optional (db *db*))
  "If connection already established... [ ? ] - any way to replace connection or keep existing connection? Return value undefined. Set up database connection to database specified by DB. Set *SQLGCON* to the corresponding connection object."
  (declare (ignore db))
  ;; all cl-sqlite code uses the db host slot value directly
  (set sqlg::*sqlgcon* t)
  t)

(defun database-defined-p (&optional (db *db*))
  (cond ((equal (db-type db) :SQLITE3)
	 ;; check if file is present
	 (probe-file (db-host db)))))

(defun db-connected-p (&optional (db *db*))
  (database-defined-p db))

(defun default-database-object ()
  t)

(defun disconnect-sql ()
  "Disconnect all database connections."
  ;; cl-sqlite code always uses with-open-database
  t)

(defun exec-single-sql-command (sqlstring &optional (db *db*))
  (sqlite:with-open-database (database (db-host db)
			      :busy-timeout *sqlite-timeout*)
    (sqlite:execute-non-query database sqlstring)))

;; * denotes parameterized version
(defun exec-single-sql-command* (sqlstring &optional (db *db*) parameters)
  "Returns nothing."
  (sqlite:with-open-database (database (db-host db)
			      :busy-timeout *sqlite-timeout*)
    (sqlite::execute-non-query* database sqlstring parameters)))

(in-package :sqlite)

;; allow above to work conveniently... just an alternative version of SQLITE:EXECUTE-NON-QUERY
;; -->> how is it different than SQLITE:EXECUTE-NON-QUERY?
;; (1) SQLITE:EXECUTE-NON-QUERY accepts &rest params

;; if parameter is of type (VECTOR (UNSIGNED-BYTE 8)) and VECTOR that contains integers in range [0,256), it is passed as a BLOB

(defun execute-non-query* (db sql &optional parameters)
  "Executes the query SQL to the database DB with given PARAMETERS. Returns nothing. See BIND-PARAMETER for the list of supported parameter types.

Example:

\(execute-non-query db \"insert into users (user_name, real_name) values (?, ?)\" \"joe\" \"Joe the User\")
"
  (declare (dynamic-extent parameters))
  (let ((stmt (prepare-statement db sql)))
    (iter (for i from 1)
          (declare (type fixnum i))
          (for value in parameters)
          (bind-parameter stmt i value))
    (step-statement stmt)
    (finalize-statement stmt)
    (values)))

(in-package :sqlg)

(defun init-db-type (&optional dbtype)
  (declare (ignore dbtype))
  nil)

;; note: if can't get column strings back, NIL as second value means column order unchanged
(defun query* (sqlstring &optional (db *db*) result-types)
  (declare (ignore result-types))
  ;; cl-sqlite EXECUTE-TO-LIST does not return field labels; we assume EXECUTE-TO-LIST guarantees order for columns/fields
  (values (sqlite:with-open-database (database (db-host db)
				      :busy-timeout *sqlite-timeout*)
	    (sqlite:execute-to-list database sqlstring))
	  nil
	  t))

(defun table-names (&key (all-p nil) (db *db*))
  (declare (ignore all-p))
  (sqlite:with-open-database (database (db-host db)
			      :busy-timeout *sqlite-timeout*)
    (mapcar
     #'(lambda (x) (car x))
     (sqlite:execute-to-list
      database
      "select name FROM sqlite_master WHERE type='table' ORDER BY name;"))))

(defun tablelabels-in-db (&key (all-p nil) (db *db*))
  (declare (ignore all-p db))
  (error "Use TABLE-NAMES"))

(defun tablep (name &optional (db *db*))
  (member name (table-names :db db) :test #'equalp))

;; advertise that SQLG-CLSQL has been loaded successfully...
(unless (member :sqlg-cl-sqlite *features*) (push :sqlg-cl-sqlite *features*))
