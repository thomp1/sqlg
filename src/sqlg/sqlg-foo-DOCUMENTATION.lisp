(in-package :sqlg)
;;;
;;; sqlg-foo-DOCUMENTATION.lisp
;;;
;;; - empty shell to hold DEFUNs which are empty apart from documentation
;;;
(defun add-row-s-pk (table field-labels values key key-label &key (db *db*))
  "Execute an INSERT command in table TABLE. Return the primary key. VALUES is a list. If KEY is NIL, rely on the database engine to generate the appropriate primary key value (e.g., using 'autoincrement' functionality)."
  (declare
   (ignore db field-labels key key-label table values))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

;; DB-SYMBOL is the symbol corresponding to the variable pointing at the database definition. SQLG:*DB* is the default value which should be used.
(defun check-db (db-symbol)
  "Check the properties of the database itself. Depending on database
being used this may include checking database integrity, verifying
that field types are legitimate, etc. Signal an error if a significant
issue is encountered with the database."
  (declare (ignore db))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun columnlabels-in-db (&optional (db *db*))
  "Return multiple values. The first value is a list of lists where
each sublist represents column labels for a table in the default
database. The second value returned is a list of strings specifying
the corresponding tables."
  (declare (ignore db))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

;; FIXME: rename as -->> table-field-labels <<--
(defun columnlabels-in-table (table &optional (db *db*))
  "Return a list of strings corresponding to the column labels for the database table represented by the string TABLE. Order of list is not specified. If TABLE does not exist, behavior is undefined (some backends may signal an error; others may not)."
  (declare (ignore db table))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defgeneric columnlabels-in-table2 (table prefixp)
  (:documentation "Return, as multiple values, two lists. The first is a list of field labels (unprefixed); the second is a list of field labels but prefixed with the table name (intended for use in an SQL statement or query involving multiple tables)."))

(defun columnlabels-in-table-no-cache (table &optional (db *db*))
  "Return a list of strings corresponding to the column labels for the database table represented by the string TABLE. Query the database, rather than relying on *STATIC-TABLES-COLUMNLABELS*."
  (declare (ignore db table))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

;; this is per-front-end since cl-sqlite handles things differently and column values
;; must be returned in a wrapper
(defun columns-where (table columns criterion &key (db *db*) unique-p)
  "Return a list of lists where each sublist corresponds to a single matching row in database table TABLE and the members of the sublist correspond to the columns specified by the strings in list COLUMNS. Return a second value, a list of column labels corresponding in position to the items in the sublists in the first value returned (see note below).

Objects in the sublists returned are either strings or integers (corresponds to clsql result-type :auto for the QUERY function).

Select only those rows which meet the criterion described by the string CRITERION (the string succeeding the SQL 'where' keyword). Example of CRITERION: the string 'clabel = a OR clabel = b'

If UNIQUE-P is true, only return unique values.

See ROWS-FROM-TABLE for a similar function.

note(s):

- rationale for returning column labels as second value: Is there any guarantee that the order of items in the sublists returned matches the order of items in list COLUMNS? 
  - CLSQL makes no such guarantee and even can return a second list of column labels ( sqlite 3.3.13 documentation also does not seem to make such a guarantee )
  - postmodern 1.14 makes no such guarantee but can return corresponding list of column labels
  - cl-sqlite makes no such explicit guarantee but doesn't return a corresponding list of column labels, suggesting order is preserved

-  not implemented (write a separate function if this functionality is desired): Use database connection CONNECTION if specified.


- ** not implemented:
needs to be implemented in a wrapper fn:

In some cases, an alternative data source may be used to provide data for some columns. (Example: using a table where firstname and lastname data are omitted (and instead are kept in a local file rather than on the database due to privacy concerns).) In such a case...

- check if columns are provided by other source (othersource column in 'columndescriptions' table)
- call that fn to get data for those columns"
  (declare (ignore columns criterion db table unique-p))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun database-connect (&optional db-spec)
  "If connection already established... [ ? ] - any way to replace connection or keep existing connection? Return value undefined. Side-effects: 

- Set up database connection to database specified by DB-SPEC. DB-SPEC is in the format documented for *SQL-DB-SPEC*. If DB-SPEC is NIL, use (DB-TYPE *DB*), (DB-HOST *DB*), (DB-NAME *DB*), *DB-USER*, and (DB-PASS *DB*).

- Set *SQLGCON* to the corresponding connection object"
  (declare (ignore db-spec))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun database-defined-p (&optional (db *db*))
  (declare (ignore db))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun db-connected-p (&optional (db *db*))
    (declare (ignore db))
    (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun default-database-object ()
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun exec-single-sql-command (sqlstring &optional (db *db*))
  "Execute a non-query."
  (declare (ignore sqlstring db))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun exec-single-sql-command* (sqlstring &optional (db *db*) parameters)
  "Execute a non-query. Parameterized version"
  (declare (ignore sqlstring db parameters))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun init-db-type (&optional dbtype)
  (declare (ignore dbtype))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun insert-string (table columns data) 
  "Return string representing complete SQL INSERT statement where COLUMNS is a list of strings specifying column labels. DATA is a list of data (the order corresponding to that of COLUMNLABELS) corresponding to a row in table TABLE."
  (declare (ignore table columns data))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun query* (sqlstring &optional database)
  "Execute the SQL query corresponding to SQLSTRING and return multiple values. The first value is a list of lists (rows). The second value is a list of corresponding column labels. If column order can't be ascertained, return NIL for the second value.

Members of each 'row' may be strings, numbers, etc. (different database front-ends may choose to return values as strings, numbers, etc.). The calling function should perform conversion to other types as needed.

Conditions: rely on the lisp SQL front-end to return an error if the SQL database engine returns an error."
  (declare (ignore sqlstring database))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))

(defun table-names (&key (all-p nil) (db *db*))
  "Return list of strings representing names of tables in the database specified by *DB*. If ALL-P is nil, exclude 'special' tables (for postgresql, this includes all tables with pg_ or sql_ as prefix."
  (declare (ignore all-p db))
  (error "Have you loaded SQLG-[dbtype] or code for SQLG-[lisp-frontend] yet?"))
