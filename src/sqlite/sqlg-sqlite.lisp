(in-package :sqlg)
;;;
;;; sqlg-sqlite.lisp: sqlite-specific sqlg code
;;;

;; sqlite doesn't use postgresql syntax facilitating insertion of multiple rows with a single insert...
(defun add-rows-string-sqlite (columns values table)
   (with-output-to-string (s)
     (dolist (vlist values)
       ;; series of INSERT statements...
       (write-string "insert into " s)
       (write-string table s)
       (write-string " (" s)
       ;; note: would be safer to double-quote column names (to accomodate more complex names)
       (dolist (column (dcu:commas-in-string-list columns))
	 (write-string column s))
       (write-string ") values " s)
       (write-char #\( s)
       (write-string (data-to-sql-string-raw vlist) s)
       (write-string ");" s))))

(defun add-row-s-pk (table field-labels values key key-label &key (db *db*))
  "Execute an INSERT command in table TABLE. Return the primary key.
VALUES is a list. If KEY is NIL, rely on the database engine to
generate the appropriate primary key value (e.g., using
'autoincrement' functionality)."
  (declare (optimize (safety 3) (debug 3))
	   (cons field-labels values)
	   (string key-label table))
  (assert (dcu::list-of-strings-p field-labels))
  (if (and key (atom-in-column-p key key-label table))
      (error 'record-exists :table table))
  (when (and key
	     (not (member key-label field-labels :test #'string=)))
    (push key values)
    (push key-label field-labels))
  (sqlite:with-open-database (database (db-host db) :busy-timeout *sqlite-timeout*)
    (sqlite:with-transaction database
      (let ((insert-string (insert-string* table field-labels)))
	(sqlite::execute-non-query* database insert-string values))
      (or key
	  (let ((raw-key-attempt
		 ;; if key was generated, this function will return ((key))
		 (sqlite:execute-to-list database "select last_insert_rowid()")))
	    (caar raw-key-attempt))))))

(defun autoincrement-p (table)
  "Determine if table TABLE has an autoincrement primary key."
  ;; SELECT COUNT(*) FROM sqlite_sequence WHERE name='yourtable';
  (> (caar (sqlg:query* (format nil "SELECT COUNT(*) FROM sqlite_sequence WHERE name='~A';" table)))
     0))

;; note: sqlite has an API for backups but it seems unlikely that the various lisp front-ends to sqlite support the API...
(defun backup (pathname &optional overwritep)
  "Perform a database backup on the default database *DB*. In the case of sqlite, this means generating a copy, specified by PATHNAME, of the sqlite file. If OVERWRITEP is true, overwrite existing file, if any, at PATHNAME."
  ;; this function should execute equivalent of:
  ;;(exec-single-sql-command "begin immediate")
  ;; [ copy file ]
  ;;(exec-single-sql-command "rollback") 
  (sqlite:with-open-database (db (db-host *db*))
    (sqlite:with-transaction db
      (let ((origin (parse-namestring (db-host *db*)))
	    (target pathname))
	(when (and (not overwritep)
		   (probe-file target))
	  (error 'file-exists))
	(dfile:delete-file-if-exists target)
	(uiop:copy-file origin target)
	;(cl-fad:copy-file origin target :overwrite overwritep)
	))))

;; DB-SYMBOL is the symbol corresponding to the variable pointing at the database definition. SQLG:*DB* is the default value which should be used.
(defun check-db (db-symbol)
  (let ((db (eval db-symbol)))
    (check-db-existence db-symbol)
    (check-db-integrity db)
    (check-db-field-types db)))

(defun check-db-existence (db-symbol)
  (let ((db (eval db-symbol)))
    ;(log:info "testing for database file at ~S~%" (db-host db))
    ;; existence check
    (unless (dfile:file-exists-p (db-host db))
      (error "Database file ~S is missing~%
The value of ~S is ~S.
"
	     (db-host db)
	     db-symbol
	     db
	     )))
  ;(log:info "existence check complete")
  )

(defun check-db-field-types (db)
  ;(log:info "db: checking field types")
  ;; field type checks; require explicit type for every field in any user table
  (let ((acceptable-types '("BLOB" "INTEGER" "NUMERIC" "REAL" "TEXT"))
	(table-names (table-names :db db))
	(ignore-tables '("sqlite_sequence")))
    ;(log:info table-names)
    (map nil #'(lambda (table-name)
		 (unless (member table-name ignore-tables :test #'string=)
		   (let ((table-info (table-info table-name db)))
		     (map nil #'(lambda (field-record)
				  (let ((type (third field-record)))
				    (unless (member type acceptable-types :test #'string=)
				      (cerror "Keep on trucking"
					      (format nil "unacceptable sqlite type ~S for field ~S in ~S table" type (second field-record) table-name)))))
			  table-info))))
	 table-names))
  ;(log:info "field type checks complete")
  )

;; integrity check
(defun check-db-integrity (db)
  ;(log:info "integrity check: not running integrity check until VM issue resolved")
  (let ((int-check-response-raw (query* "PRAGMA integrity_check" db)))
    ;(log:info int-check-response-raw)
    (unless (string= (caar int-check-response-raw)
		     "ok")
      (error "sqlite integrity_check failed")))
  ;(log:info "integrity check complete")
  )

(defun columnlabels-in-db (&optional (db *db*))
  (let ((tables (table-names :db db)))
    (values (mapcar #'(lambda (table) (columnlabels-in-table table db))
		    tables)
	    tables)))

;; return, as multiple values, the labels for the table and, as a second value, if prefixp is true, the prefixed (SQL syntax) labels
(defgeneric columnlabels-in-table2 (table prefixp))

(defmethod columnlabels-in-table2 (table (prefixp null))
  (columnlabels-in-table table))

(defmethod columnlabels-in-table2 (table (prefixp t))
  (let ((field-labels (columnlabels-in-table2 table nil)))
    (values field-labels
	    (mapcar #'(lambda (label)
			(concatenate 'string table "." label))
		    field-labels))))

(defun columnlabels-in-table (table &optional (db *db*))
  (let ((position (position table *static-tables* :test #'string=)))
    (cond (position
	   (nth position *static-tables-columnlabels*))
	  (t
	   #+:SQLG-STATIC-TABLE-WARNINGS (warn (format nil "SQLG: ~A not in *STATIC-TABLES* - consider updating *static-tables*" table))
	   (columnlabels-in-table-sqlite table db)))))

(defun columnlabels-in-table-no-cache (table &optional (db *db*))
  (columnlabels-in-table-sqlite table db))

(defun columnlabels-in-table-sqlite (table &optional (db *db*))
  (declare (optimize (speed 3) (safety 1))
	   (string table))
  (mapcar #'(lambda (x)
	      ;; ODBC and SQL specifications indicate that table names must only contain letter, digit, and/or underscore characters
	      ; would be nice to be able to do this... (the simple-base-string
		(second x))
	  (table-info table db)))

(defun insert-or-replace-string (table columns data)
  "Return string representing complete SQL INSERT OR REPLACE statement were COLUMNS is a list of strings specifying column labels. DATA is a list of data (the order corresponding to that of COLUMNS) which should be updated in that row."
  ;; CSTREAM: column string stream
  (let ((cstream (make-string-output-stream)))
    (mapcar #'(lambda (c)
		(write-string (dcu:concs "'" c "', ") cstream))
	    columns)
    (let ((dstring (data-to-sql-string-raw data))
	  (cstring
	   (string-right-trim " ," (get-output-stream-string cstream))))
      (format nil "INSERT OR REPLACE INTO ~A ( ~A ) VALUES ( ~A );" table cstring dstring))))

;; INSERT-STRING02
;; slower than INSERT-STRING
;; (defun insert-string01 (table columns data)
;;   "Return string representing complete SQL INSERT statement where COLUMNS is a list of strings specifying column labels. DATA is a list of strings representing the data (the order corresponding to that of COLUMNLABELS) which should be updated in that row."
;;   ;; FIXME: optimize -- compare with INSERT-STRING* using a single output stream

;;   ;; optimize this function
;;   (declare ;;(optimize (speed 3) (safety 1) (debug 0))
;; 	   (string table)
;; 	   (cons columns data))
;;   (the string
;;     ;; CSTRING: column string 
;;     (let ((cstring
;; 	   (with-output-to-string (cstream)
;; 	     (mapcar   
;; 	      #'(lambda (c)
;; 		  ;; sqlite3 is fine with single quotes
;; 		  (format cstream "'~A', " c)) 
;; 	      columns)))
;; 	  (valuesstring 
;; 	   ;; DATA-TO-SQL-STRING-RAW expects data to be a list of *strings*
;; 	   (data-to-sql-string-raw data)))
;;       (assert (not (noes valuesstring)))
;;       ;; this cannot be followed by a carriage return
;;       ;; - cl-sqlite is sensitive to any whitespace after the semicolon when this is used as a single command
;;       (format nil "INSERT INTO ~A ( ~A ) VALUES ( ~A );" 
;; 	      table 
;; 	      (string-right-trim " ," cstring)
;; 	      valuesstring))))

;; INSERT-STRING02
(defun insert-string (table columns data)
  "Return string representing complete SQL INSERT statement where COLUMNS is a list of strings specifying column labels. DATA is a list of strings representing the data (the order corresponding to that of COLUMNLABELS) which should be updated in that row."
  ;; optimize this function
  (declare ;;(optimize (speed 3) (safety 1) (debug 0))
   (string table)
   (cons columns data))
  (the string
    ;; cl-sqlite is sensitive to any whitespace after the semicolon when this is used as a single command
    (with-output-to-string (s)
      (write-string "INSERT INTO " s)
      (write-string table s)
      (write-string " ( " s)
      (dcu::list-to-stream*
       columns s
       #'(lambda (field-label stream)
	   (format stream "'~A'" field-label))
       ", ")
      (write-string " ) VALUES ( " s)
      (data-to-sql-stream-raw data s)
      (write-string " );" s))))

(defun primary-key-columnlabel (table)
  "Return a string representing the SQL type for the column."
  (declare (string table))
  ;; fields for TABLE-INFO are 'cid', 'name', 'type', 'notnull', 'dflt_value', 'pk'
  (let ((table-info (table-info table)))
    (dolist (field-description table-info)
      (let ((primary-key-p (> (elt field-description 5) 0)))
	(if primary-key-p
	    (return (elt field-description 1)))))))

(defun table-info (table &optional (db *db*))
  "Return structure of database table TABLE as multiple values. The first value returned is a list of lists. Each list contains the columnid (cid), the column label (name), a string representing the data type in column (type), the notnull value for the column, the default value for the column (dflt_value), and the pk (primary key?) value for the column.

notes: 

postgresql: equivalent = ?

sqlite3: rely on pragma table_info; each list contains columnid (cid), column label (name), data type in column (type), the notnull value for the column, the default value for the column (dflt_value), and the pk (primary key?) value for the column."

  ;; sqlite3:
  ;; currently pragma table_info returns list of lists
  ;; each sublist has column name at second position

  
  ;; only works with sqlite3?
  (locally
      (declare (sb-ext:muffle-conditions style-warning))
    (query* (format nil "pragma table_info(~A);" table) db)))


;; advertise that SQLG-SQLITE has been loaded successfully...
(unless (member :sqlg-sqlite *features*) (push :sqlg-sqlite *features*))
