(in-package :sqlg)

(define-condition nonexistent-record (error)
  ((table :initarg :table
	  :initform nil
	  :reader tableread))
  (:documentation
   "This should be signalled if an attempt was made to refer to, or
   access, a database record which did not exist at the time of the
   reference.")
  (:report (lambda (condition stream)
	     (let ((table (tableread condition)))
	       (format stream "An attempt was made to refer to, or access, a database record (in table ~A) which did not exist at the time of the reference" table)))))

(define-condition record-exists (error)
  ((table :initarg :table
	  :initform nil
	  :reader tableread))
  (:documentation
   "This should be signalled if an attempt was made to create a new
   record where such a record already exists.")
  (:report (lambda (condition stream)
	     (let ((table (tableread condition)))
	       (format stream "An attempt was made to create a new record  (in table ~A) where such a record already exists." table)))))
