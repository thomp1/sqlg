(in-package :sqlg)
;;
;; static.lisp: cached database data
;;

(defparameter *static-tables* nil
  "List of strings, each representing a 'static' table (see
  DECLARE-STATIC for more on the nature of a static table). Manipulate
  this value only through DECLARE-STATIC.")

(defparameter *static-tables-columnlabels* nil
  "List of lists of strings, each list representing field labels for
  the corresponding table specified in *STATIC-TABLES*.")

(defun clear-static ()
  (setf *static-tables-columnlabels* nil)
  (setf *static-tables* nil))

(defun declare-static (&rest names)
  "Declare that tables specified by NAMES (a list of strings) will not
change with respect to structure (number and names of tables and
number and names of columns/fields) over the lifetime of the
executable utilizing SSQLFS. If table has already been declared
static, refresh field labels for that table."
  (declare (list names))
  (map nil #'(lambda (tablelabel)
	       (declare (string tablelabel))
	       (let ((index (position tablelabel *static-tables*
				      :test #'string=)))
		 (cond (index
			(setf (elt *static-tables-columnlabels* index)
			      (columnlabels-in-table-no-cache tablelabel)))
		       (t
			(push tablelabel *static-tables*)
			(push (columnlabels-in-table-no-cache tablelabel) *static-tables-columnlabels*)))))
       names))

(defun is-static? (table-label)
  (member table-label *static-tables* :test 'string=))

(defun refresh-static ()
  "Update *STATIC-TABLES-COLUMNLABELS* using current database tables."
  (error "Use declare-static unless there is a clear reason to use refresh-static")
  (map-into *static-tables-columnlabels*
	    #'(lambda (table-label)
		(columnlabels-in-table-no-cache table-label))
	    *static-tables*))
