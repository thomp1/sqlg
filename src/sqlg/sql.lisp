(in-package :sqlg)
;;;
;;; shared-sql.lisp: code for interfacing with a SQL database
;;;

;;;
;;; note(s):
;;; - documentation for sql package-specific stuff is in sqlg-foo-DOCUMENTATION.lisp
;;; - the primary goal of this component of the sqlg package is to facilitate avoiding sql package-specific references in other code
;;;


;; FIELD-LABELS: always a list of strings representing fields in a database table
;; VALUES: always a list representing values corresponding to fields in FIELD-LABELS


;;
;; variables specific to sql.lisp
;;
(defvar *default-database-type* :sqlite3
  "Database type to use (currently only tested with :sqlite).")

(defvar *vis-col-order-concise*
  nil
  "An alist representing the order in which column data should be presented for a given row in a table. The first member of each cons (in the alist) is the table name; the second member of the cons is an ordered list of column labels.

If a table is not described in *VIS-COL-ORDER-...*, all columns may be shown, in any order.

Columns to show and order for a concise 'summary' page of table data are specified here. Columns to show and order for an page which exposes detailed table data are specified in *vis-col-order-full*.

If a column label 'x' is not specified in the ordering for a table but that table has a column 'x', data for that column are not shown.")

(defvar *vis-col-order-full*
  nil
  "An alist representing the order in which column data should be presented for a given row in a table. The first member of each cons (in the alist) is the table name; the second member of the cons is an ordered list of column labels.

If a table is not described in *VIS-COL-ORDER-...*, all columns may be shown, in any order.

Columns to show and order for an page which exposes detailed table data are specified here. Columns to show and order for a concise 'summary' page of table data are specified with *VIS-COL-ORDER-CONCISE*.

If a column label 'x' is not specified in the ordering for a table but that table has a column 'x', data for that column are not shown.")

;; FOO* indicates version of function FOO which uses a parameterized SQL query (* does not indicate a 'safe' function in the sense of the function call encompassing reasonable sanity checks, types checks, etc.)

;; note: 'ADD' used in name of function instead of 'INSERT' to avoid confusion with lisp INSERT - alternatively could use 'INSERT-ROW' or something along those lines...

(defun add-row* (table columnlabels row &optional (db *db*))
  "Execute an insert using a parameterized SQL statement. The list of strings COLUMNLABELS includes all field labels. ROW is a list representing the corresponding data for each row. Each member of ROW is either a string or a vector with elements of type (unsigned-byte 8). The return value of this function is undefined."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (cons columnlabels row)
	   (string table))
  ;; parameterized version of EXEC-SINGLE-SQL-COMMAND...
  (exec-single-sql-command* (insert-string* table columnlabels) db row))

;; FOO-S indicates version of function FOO which uses a parameterized SQL query and executes reasonable sanity checks (e.g., type checks, etc.)
(defun add-row-S (table field-labels values key key-label)
  "Return value is undefined. VALUES is a list. If KEY is NIL, rely on database engine to generate appropriate primary key value. See also ADD-ROW-S-PK."
  (declare (optimize (safety 3) (debug 3))
	   (cons field-labels values)
	   (string key-label table))
  (assert (dcu::list-of-strings-p field-labels))
  (if (and key (atom-in-column-p key key-label table))
      (error 'record-exists :table table))
  (add-row* table field-labels values))

;; testing to replace complex version
(defun add-or-update-mm-row (keylabel1 keylabel2 key1 key2 mmtable &key columnlabels (db *db*) values)
  "Return value is undefined. VALUES is a list of strings. Add or upate row in many-to-many table MMTABLE where column KEYLABEL1 has value KEY1 and column KEYLABEL2 has value KEY2.

KEY1 - a string
KEY2 - a string

?? Add additional data, if any, in list VALUES where position in list corresponds to the column with label specified by the string in the corresponding position in list COLUMNLABELS."
  (declare (list columnlabels values)
	   (string key1 key2 keylabel1 keylabel2 mmtable))
  (cond ((not (rows-where mmtable
			  (sqle:pairs keylabel1 key1 keylabel2 key2)
			  nil
			  db)
	      ;(mm-record key1 keylabel1 key2 keylabel2 mmtable :db db)
	      )
	 (add-row* mmtable
		  (cons keylabel1 (cons keylabel2 columnlabels))
		  (cons key1 (cons key2 values)) db))
	(t
	 ;; only add keylabel fields if not present...
	 (multiple-value-bind (new-values new-labels)
	     (dcu::replace-or-add* values columnlabels
				   (list keylabel1 keylabel2)
				   (list key1 key2))
	   (update* mmtable
		    new-labels
		    new-values
		    (se:pairs keylabel1 key1 keylabel2 key2)
		    db)))))

;; FIXME: use ADD-OR-UPDATE-MM*
(defun add-or-update-row-mm* (keys keylabels mmtable &key columnlabels (db *db*) values)
  (add-or-update-mm-row
   (first keylabels)
   (second keylabels)
   (first keys)
   (second keys)
   mmtable
   :columnlabels columnlabels
   :db db
   :values values))

;; Keys and keylabels may force a bit of extra consing on calling function. Consider simplifying by changing args to (key1 keylabel1 key2 keylabel2 mmtable &key columnlabels (db *db*) values)
(defun add-or-update-mm* (keys keylabels mmtable &key columnlabels (db *db*) values)
  "VALUES is a list of strings. Return value is undefined.
ADD-OR-UPDATE-MM* is intended as the 'goto function' used for
add/update requests for many-to-many tables."
  (add-or-update-mm-row
   (first keylabels)
   (second keylabels)
   (first keys)
   (second keys)
   mmtable
   :columnlabels columnlabels
   :db db
   :values values))

(defun all-rows (table &optional field-labels (db *db*))
  "Return, as multiple values, two lists. The first list represents
the rows in table TABLE. The second list represents the corresponding
column labels."
  ;; SELECT * is no good since some front-ends don't return column labels as second value...
  ;; (query (format nil "select * from ~A;" table) db)
  (columns table
	   (or field-labels (columnlabels-in-table table db))
	   :db db))

;; this function introduces opacity regarding whether a record exists for the criterion or not;
;; --> where is opacity? could NIL correspond to the value in a cell? If so, no way to interpret whether NIL corresponds to NULL or simply to absence of a record. --> expect-datum-where--safe addresses this
;; thus, this function should only be used in a deliberate manner
(defun %expect-datum-where (table column criterion &optional (db *db*))
  "Expect no match or a single record to match. The search is in the
table TABLE where CRITERION specifies the search. The value returned
is from the field named COLUMN."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string table column criterion))
  (let ((atoms (column-where table column criterion :db db)))
    (declare (list atoms))
    (unless (< (length atoms) 2)
      (error "Expected a single record to match."))
    (first atoms)))

(defun expect-datum-where--safe (table column criterion &optional (db *db*))
  "Expect a single record to match. The search is in the table TABLE where CRITERION specifies the search. The value returned is from the field named COLUMN."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string table column criterion))
  (multiple-value-bind (atom recordp)
      (datum-where2 table column criterion db)
    (unless recordp
      (error "Expected at least one record to match"))
    atom))

;; safe version distinguishing between result where cell value is NULL
;; and absence of a database record
(defun datum-where2 (table column criterion &optional (db *db*))
  "Expect either no match or a single record to match. The search is
in the table TABLE where CRITERION specifies the search. Multiples
values are returned. The first value corresponding to the field named
COLUMN -- if the specified record exists, this is the value of the
specified field. If the second value is a true value, this indicates
that the first value corresponds to a database record. If the second
value is NIL, this indicates that a matching database record does not
exist."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string table column criterion))
  (let ((atoms (column-where table column criterion :db db)))
    (declare (list atoms))
    (unless (< (length atoms) 2)
      (error "Expected a single record to match."))
    (values (first atoms)
	    atoms)))

;; unsafe as it silently discards multiple matches in situation of multiple matches

;; FIXME: versus DATUM-WHERE?
(defun %atom-where (table column criterion &optional (db *db*))
  "The value returned is either an integer, a string, or NIL. Use EXPECT-DATUM-WHERE unless silently discarding multiple matches is acceptable."
  ;; FIXME: claim of value returned being an integer, string, or NIL
  ;;        needs to be evaluated in light of each db interface
  ;; - postmodern:
  ;;     returns integer and real values as string objects
  ;; - clsql:
  ;;     ?
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string table column criterion))
  (caar (columns-where table (list column) criterion :db db)))

(defun atom-in-column-p (value columnlabel table &optional (db *db*))
  "Return a true value if value VALUE exists in column COLUMNLABEL in table TABLE. Otherwise, returns NIL."
  (declare (string columnlabel table value))
  (query* (with-output-to-string (s)
	    (write-string "SELECT " s)
	    (write-string columnlabel s)
	    (write-string " from " s)
	    (write-string table s)
	    (write-string " where " s)
	    (write-string columnlabel s)
	    (write-string " = '" s)
	    (write-string value s)
	    (write-string "';" s))
	  db))

;; FIXME: behavior of function doesn't match documentation; should be ONE-OF-ATOMS-NOT-IN-COLUMN-P or something similar: AN-ATOM-NOT-IN-COLUMN-P ?
(defun atoms-in-column-p (values columnlabel table &optional (db *db*))
  "If a member of list VALUES does not exist in column COLUMNLABEL in
table TABLE, return the first member of the list meeting that
criterion. Otherwise, (if all members of keys are in table) return
NIL."
  (dolist (v values)
    (if (not (atom-in-column-p v columnlabel table db))
	(return v))))

;; FIXME: better named 'db-ready'? or 'check-and-connect' ?

;; FIXME: this should be defined precisely (currently unimplemented) : Return a true value if things are ready to go. Return nil otherwise.
(defun check-sql ()
  "Ensure that the default database (*DB*) is 'ready to rumble'.
Checks whether default sql database is present (the connection
specification is SQLG:*SQL-DB-SPEC* and type of database is specified
by SQLG:*SQL-DB-TYPE*). If a connection is not already established,
establishes the connection."
  ;; check if database type is implemented (see documentation for DEF-DB-PARAMS)
  (assert (member (db-type *db*) (list :sqlite3 :postgresql)))
  ;; ensure default database exists
  (cond ((not (database-defined-p))
	 ;; allow user to continue since DATABASE-DEFINED-P functionality isn't guaranteed
	 ;; - e.g., CLSQL with POSTGRESQL-SOCKET can't probe for database
	 (cerror "Keep on trucking"
		 (format nil "WARNING: sql database may not be present (host: ~A user: ~A name: ~A pass: ~A~%~%Ensure the database is configured and the program is configured to recognize it." (db-host *db*) (db-user *db*) (db-name *db*) (db-pass *db*))))
	;; establish connection if not already present
	(t
	 (ensure-connection)
	 (ensure-*sqlgcon*-set))))

;; note(s): UNIQUE-P relies on SQL functionality ('distinct')
(defun column (table column &key unique-p (db *db*))
  "Return list representing data in column in table TABLE with label
COLUMN. If UNIQUE-P is true, only return unique values. If COLUMN
doesn't exist, return NIL. DB is a db structure (see *DB*
documentation)."
  (when (columnlabel-in-table-p column table db)
    (let ((raw-data (query* (dcu:concs "select "
				       (if unique-p "distinct " "")
				       (sqlg::field-labels-string (list column))
				       " from "
				       table ";")
			    db)))
      (if raw-data
	  ;; get first item from each list
	  (mapcar (lambda (x) (car x))
		  raw-data)))))

(defun column-where (table column criterion &key (db *db*) unique-p)
  "Return a list. The list represents data in database table TABLE in column with label COLUMN.

Select only those rows which meet the criterion described by the string CRITERION (the string succeeding the SQL 'where' keyword). Example of CRITERION: the string 'clabel = a OR clabel = b'

If UNIQUE-P is true, only return unique values."
  (assert (stringp column))
  (when (columnlabel-in-table-p column table db)
    (let ((raw-data (query* (dcu:concs "select "
				       (if unique-p
					   "distinct "
					   "")
				       (sqlg::field-labels-string (list column))
				       " from " table " where " criterion ";")
			    db)))
      (mapcar #'(lambda (x) (car x))
	      raw-data))))

(defun columnlabel-in-table-p (c table &optional (db *db*))
  "Return a true value if columnlabel C corresponds to a column in the table TABLE in the default database."
  ;; SQL spec limits table and column labels to contain only letter, digit, and underscore characters
  ;(declare (simple-base-string table c))
  (member c
	  (columnlabels-in-table table db)
	  :test 'equal))

(defun columnlabels-in-table-p (c table &optional (db *db*))
  "Return a true value if columnlabels C correspond to columns in the table TABLE in the default database."
  (not (member nil
	       (mapcar #'(lambda (x)
			   (columnlabel-in-table-p x table db))
		       c))))

(defun columnlabels-not-in-table (table some-list &optional (db *db*))
  "Return list composed of members of SOME-LIST which aren't column
labels for columns in database table TABLE."
  (set-difference some-list (columnlabels-in-table table db) :test 'equal))

;; FIXME: is order of columns guaranteed?

;; returning second value is significant for other code
(defun columns (table columns &key (db *db*) unique-p)
  "Return a list containing the data in the column in the database
  table TABLE with labels specified by the strings in list COLUMNS as
  a list. If UNIQUE-P is true, don't return duplicates. If a member of
  COLUMNS is not a column in the table, return NIL.

Return a second value, the corresponding 'column labels', as a list.

** not implemented:
needs to be implemented in a wrapper fn:

In some cases, an alternative data source may be used to provide data for some columns. (Example: using a table where firstname and lastname data are omitted (and instead are kept in a local file rather than on the database due to privacy concerns).) In such a case...

- check if columns are provided by other source (othersource column in 'columndescriptions' table)
- call that fn to get data for those columns"
  (assert (stringp table))
  (if (columnlabels-in-table-p columns table db)
      ;; CSTRING: sql string 'column1,column2' specifying the columns
      (let ((cstring (field-labels-string columns)))
	;; depending on front-end QUERY may not return columnlabels as second value
	(multiple-value-bind (rows columnlabels?)
	       (query* (dcu:concs "select "
			      (if unique-p "distinct " "")
			      cstring " from " table ";")
		       db)
	  (values rows (if columnlabels?
			   columnlabels?
			   columns))))
      (values nil nil)))

(defun columns-from-table-where-core (table columns criterion &key (db *db*) unique-p)
  "COLUMNS-FROM-TABLE-WHERE but makes no guarantees about returning anything except the first value."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string criterion table)
	   (cons columns))
  (let ((qstring
	 (with-output-to-string (s)
	   (write-string "select" s)
	   (write-string (if unique-p " distinct " " ") s)
	   ;; make sql string 'column1,column2 ...' specifying the columns
	   (let ((clength-1 (1-
			     (the fixnum
			       (list-length columns)
			       ))))
	     ;;(declare ((unsigned-byte 8) clength-1))
	     (do ((x 0 (1+ x)))
		 ((= x clength-1)
		  (write-string (nth x columns) s))
	       ;; FIX UPPER LIMIT ON TABLE WIDTH (COLUMN #) AT:
	       ;;   2^8: (unsigned-byte 8)
	       ;;   536870911 (fixnum; SBCL)
	       (declare (fixnum x))
	       (write-string (nth x columns) s)
	       (write-char #\, s)))
	   (write-string (dcu:concs " from " table " where " criterion ";") s)
	   )))
    (query* qstring db)))

;; COUNT shouldn't be used as CL fn already exists with that name
(defun count-as (table where-clause)
  "Count rows in table TABLE matching WHERE-CLAUSE."
  (declare (string table))
  ;; w/sqlite, the query returns ((663)) or ((1))...
  (first (first (query* (format nil "SELECT COUNT(*) FROM ~A WHERE ~A;" table where-clause)))))

(defun create-table-string (table columns &optional types)
  "Return string representing a SQL statement for adding a table TABLE to SQL database where COLUMNS is a sexp with strings specifying data types. The (optional) type of each column is specified in the sexp TYPES. If types aren't supplied and the database type sqlg::(db-type *db*) is :postgresql, a type of 'text' will be used. Note that, with postgresql, type specifications are recommended. Note tha sqlite is typeless: types are optional."
  (let ((types (if types
		   types
		   (cond ((or (equal (db-type *db*) :sqlite)
			      (equal (db-type *db*) :sqlite3))
			  types)
			 ((equal (db-type *db*) :postgresql)
			  (make-list (list-length columns) :initial-element "text"))
			 (t
			  (error "Check documentation for db and then code for appropriate generic type"))))))
    (with-output-to-string (s)
      (write-string "create table \"" s)
      (write-string table s)
      (write-string "\" " s)
      ;; columns
      (if columns
	  (progn
	    (write-char #\( s)
	    (do ((column (pop columns) (pop columns))
		 (type (pop types) (pop types)))
		((not columns)
		 ;; postgresql cares about extra comma at end
		 (write-string (dat-cl-utils::doublequote column) s)
		 (when type
                   (write-char #\Space s)
		   (write-string type s)))
	      (write-string (dat-cl-utils::doublequote column) s)
	      (when type
                (write-char #\Space s)
		(write-string type s))
	      ;; does sql care if extra comma?
	      (write-char #\, s))
	    (write-char #\) s)))
      (write-string ";" s))))

;; note: potentially heavily used; should optimize for speed
(defun data-to-sql-stream-raw (data stream)
  "Write to stream STREAM a comma-separated list of values corresponding to those objects (each object can either be a string or NIL) in the list DATA. Order is guaranteed. Each string in DATA is represented as an SQL constant (surrounded by apostrophes).

SQL-unfriendly characters (apostrophes, etc.) in the representations of objects are modified so that the representations are suitable for inclusion in an SQL statement. The objects may be of any type. (Overall goal: Make each datum 'sql-safe' and return string suitable for inclusion in an sql statement/command)."
  (declare (optimize (speed 3) (safety 3) (debug 1)))
  (assert (listp data))
  (dcu::list-to-stream*
   data stream
   #'(lambda (d s)
       ;; deal with apostrophes; possible approaches:
       ;; 1. (format dstream "'~A', " (apostrophe-to-double d))
       ;; 2. apostrophe-to-double only handles strings - a problem if d is the object ''(joejoe)
       (write-char #\' s)
       (write-string (if d
			 (dcu:apostrophe-to-double d)
			 "")
		     s)
       (write-char #\' s))
   ", "))

;; FIXME:  rename DATA-TO-SQL-CONSTANTS-STRING - more descriptive...
;; slower
;; (defun data-to-sql-string-raw-OLD (data)
;;   "Return a string representing the objects (each object can either be a string or NIL) in the list DATA. In the returned string, each string in DATA is separated by a comma from any adjacent strings. Each string in DATA is represented as an SQL constant (surrounded by apostrophes).

;; SQL-unfriendly characters (apostrophes, etc.) in the representations of objects are modified so that the representations are suitable for inclusion in an SQL statement. The objects may be of any type. (Overall goal: Make each datum 'sql-safe' and return string suitable for inclusion in an sql statement/command).

;; note(s):
;;   - order is guaranteed
;;   - items are separated by comma and single whitespace
;;   - heavily used; should optimize for speed"
;;   (declare (optimize (speed 3) (safety 3) (debug 1))
;; 	   ;;(list data)
;; 	   )
;;   (assert (listp data))
;;   (let ((dstring
;; 	 (with-output-to-string (dstream)
;; 	   (dolist (d data)
;; 	     ;; deal with apostrophes; possible approaches:
;; 	     ;; 1. (format dstream "'~A', " (apostrophe-to-double d))
;; 	     ;; 2. apostrophe-to-double only handles strings - a problem if d is the object ''(joejoe)
;; 	     (write-char #\' dstream)
;; 	     (write-string (if d
;; 			       (apostrophe-to-double d) 
;; 			       "")
;; 			       dstream)
;; 	     (write-string "', " dstream)
;; 	     ;; any other 'problem' characters? to intercept?
;; 	     )
;; 	   ;;mapcar maps over data
;; 	   )))
;;     ;; seems clumsy somehow...
;;     (string-right-trim " ," dstring)))

;; data-to-sql-string-raw-NEW
(defun data-to-sql-string-raw (data)
  "Return a string representing the objects (each object can either be a string or NIL) in the list DATA. In the returned string, each string in DATA is separated by a comma from any adjacent strings. Each string in DATA is represented as an SQL constant (surrounded by apostrophes).

SQL-unfriendly characters (apostrophes, etc.) in the representations of objects are modified so that the representations are suitable for inclusion in an SQL statement. The objects may be of any type. (Overall goal: Make each datum 'sql-safe' and return string suitable for inclusion in an sql statement/command).

note(s):
  - order is guaranteed
  - items are separated by comma and single whitespace
  - heavily used; should optimize for speed"
  (declare (optimize (speed 3) (safety 3) (debug 1)))
  (assert (listp data))
  (with-output-to-string (s)
    (data-to-sql-stream-raw data s)))

(defun dbparams ()
  "Return human-readable string describing database connection
parameters (for default database) of potential interest."
  (with-output-to-string (s)
    (format s "host: ~A~%name: ~A~%user: ~A~%type: ~A~%" (db-host *db*) (db-name *db*) (db-user *db*) (db-type *db*))))

;; FIXME: 'set-*db*' would be clearer name
(defun def-db-params (type &key database host password user)
  "Set parameters for default database *DB*. TYPE can be :sqlite3 or :postgresql. DATABASE is the name of the database. HOST can be a path (sqlite3), a string representing an IP (postgresql - e.g., ''127.0.0.1''), NIL (postgresql - connect to local host), ... USER can be a string (e.g., for postgresql, it might be 'posgres') or nil.

Sets *DB* slot in the following manner:
- TYPE slot value to TYPE
- HOST slot value to HOST
- NAME slot value to DATABASE
- USER slot value to USER
- PASS slot value to PASSWORD

Note that functions calling DEF-DB-PARAMS should consider calling (sqlg::load-db-type-code type backend) to ensure appropriate code is loaded."
   (unless *db*
     (setf *db* (make-db)))
   (setf (db-user *db*) user)
   (setf (db-type *db*) type)
   (setf (db-pass *db*) password)
   (setf (db-host *db*) host)
   (setf (db-name *db*) database))

(defun delete-all (table &optional (db *db*))
  "Delete all records in table TABLE. Return value undefined."
  ;; alternative: drop table and recreate
  (exec-single-sql-command
   (format nil "delete from ~A;" table)
   db))

;; This provides additional flexibility relative to DELETE-WHERE* by permitting
;; an arbitrary string after WHERE
(defun delete-from* (table expr values &optional (db *db*))
  "Delete records in database table TABLE. EXPR is a string,
presumably parameterized, anticipating the values specified by the
sequence VALUES. EXPR is included verbatim after the DELETE FROM
<tablename> component of the SQL command. Return NIL."
  (exec-single-sql-command*
   (with-output-to-string (s)
     (write-string "DELETE FROM " s)
     (write-string table s)
     (write-char #\Space s)
     (write-string expr s))
   db values)
  nil)

(defun delete-string* (table columnlabels &optional (andp t))
  "Return string representing complete parameterized SQL INSERT statement where COLUMNLABELS is a list of strings specifying column labels."
  (declare (string table)
	   (cons columnlabels))
  (the string
       ;; cl-sqlite is sensitive to any whitespace after the semicolon when this is used as a single command
       (with-output-to-string (s)
	 (write-string "DELETE FROM " s)
	 (write-string table s)
	 (write-string " WHERE " s)
	 (sqle::p-labels columnlabels s (if andp :and :or))
	 (write-char #\; s))))

(defun %delete-where (table expr &optional (db *db*))
  "Delete records in SQL database table TABLE which are selected with
the SQL expression EXPR. Returns NIL."
  (exec-single-sql-command
   (delete-from-table-where-string table expr)
   db)
  nil)

(defun delete-where* (table columnlabels values &optional (db *db*) (andp t))
  "Delete records in database table TABLE which match the set of field
label/value pairs specified by COLUMNLABELS and VALUES. Return NIL."
  (exec-single-sql-command* (delete-string* table columnlabels andp) db values)
  nil)

(defun delete-from-table-where-string (table expr)
  (format nil "delete from ~A WHERE ~A;"
	  table expr))

(defun %delete-from-tables-where (tables expr &optional (db *db*))
  "Delete records in database tables TABLES where row conforms to sql expression EXPR."
  (dolist (table tables)
    (%delete-where table expr db)))

(defun dump-table (table &optional (db *db*))
  "Returns multiple values. The first is a list of sublists, corresponding to the rows in table. The second value returned is a list of column labels, corresponding to the sublists in the first value.

note(s):
 -  See also PPRINT-TABLE-SQL."
  ;; note(s):
  ;; - we don't use SELECT * FROM FOO since
  ;;   - QUERY only returns a single value with POSTMODERN
  ;;   - QUERY only returns a single value with CL-SQLITE

  ;; - how is order of columns defined for select * from widgets; ? - use :alists, :str-alists, or :str-alist
  ;; - should have two query commands - one which returns second value; one which returns only first
  (let ((clabels (columnlabels-in-table table)))
    (multiple-value-bind (rows columnlabels)
	(query* (format nil "select ~A from ~A;"
			(labels-string clabels)
			table)
		db)
      (values rows
	      (if columnlabels
		  columnlabels
		  clabels)))))

(defun duplicates-by-column (column table &optional (db *db*))
  (duplicates-by-column-FAST column table db))

(defun duplicates-by-column-FAST (column table db)
  (query* (format nil "select ~A, count(~A) as dup_count FROM ~A GROUP BY ~A HAVING ( COUNT(~A) > 1);"
		  column column table column column)
	  db))

(defun duplicates-by-columns-except (except-column table &optional db)
  "Look for records which contain same values in all columns in table TABLE except EXCEPT-COLUMN."
  (duplicates-by-columns (remove except-column (columnlabels-in-table table db) :test #'string=) table))


;; FIXME: move core to dat-cl-utils -- this is a generic routine for identifying 'somewhat-duplicate-rows'
(defun duplicates-by-columns (columns table)
  "Look for records which contain same values in columns COLUMNS in table TABLE. Return a list of lists - each sublist is a set of records where the values in COLUMNS are the same."
  ;; FIXME: having taken a course on algorithms would be useful here...

  ;; one possible approach: do comparison one column at a time, limiting things as we go...
  ;; 1. group based on duplicates in first column - then group based on duplicates in second column, etc...
  (multiple-value-bind (rows all-columnlabels)
      (dump-table table)
    (declare (special all-columnlabels))
    (let ((col-indices (mapcar #'(lambda (col) (position col all-columnlabels :test #'string=)) columns)))
      (values (duplicates-by-columns-helper rows col-indices)
	      all-columnlabels))))

;; test: this works:
;; (sqlg::duplicates-by-columns-helper '((1 2 3 4 5 6 :key1) (1 2 3 4 5 6 :key2) (1 2 3 4 5 6 :key3) (2 3 4 5 6 7 :key4) (2 3 4 5 6 7 :key5) (2 3 4 5 6 7 :key6) (3 4 5 6 7 8 :key7) (4 5 6 7 8 9 :key8)) '(0 1 2 3 4 5))
(defun duplicates-by-columns-helper (rows col-indices)
  "Initially group rows before calling generic function."
  (declare (special all-columnlabels))
  (when rows
    ;; GROUPED-ROWS: ROWS-WITH-DUPLICATES-AT: grouped rows with duplicates at position of CURRENT-CLABEL
    (let ((grouped-rows
	   (let ((current-col-index (pop col-indices)))
	     (remove-if #'(lambda (some-list)
			    (< (length some-list) 2))
			(funcall #'dcu:group-by2 current-col-index rows)))))
      (if col-indices
	  ;; approach 1. throw away grouping info:
	  ;;(duplicates-by-columns-helper (dat-cl-utils::flatten-once grouped-rows) clabels)
	  ;; approach 2. don't throw away grouping information:
	  (group-grouped-at grouped-rows col-indices)
	  grouped-rows))))

;; test: (sqlg::group-grouped-at '(((NIL 0 "F")  (NIL 0 NIL))) '(0))
;; - should return same lis
;; test: (sqlg::group-grouped-at '(((NIL 0 "F")  (NIL 0 NIL))) '(0 1))
;; - should return same list
;; test: (sqlg::group-grouped-at '(((NIL 0 "F")  (NIL 0 NIL))) '(0 1 2))
;; - should return NIL
(defun group-grouped-at (grouped-rows indices &key (test #'equalp))
  "Given list GROUPED-ROWS (a list of lists of lists). Each sublist represents a group. Within each group, group based on applying test TEST, comparing elements at the Nth position, where N is obtained, sequentially from list of integers INDICES."
  (when grouped-rows
    (dolist (index indices)
      ;; for each group, replace with new group(s)
      (setf grouped-rows
	    (group-grouped-at1 grouped-rows index :test test)))
    grouped-rows))

;; test: (sqlg::group-grouped-at1 '(((NIL 0 "F")  (NIL 0 NIL))) 0)
;; - should return same lis
;; test: (sqlg::group-grouped-at1 '(((NIL 0 "F")  (NIL 0 NIL))) 1)
;; - should return same list
;; test: (sqlg::group-grouped-at1 '(((NIL 0 "F")  (NIL 0 NIL))) 2)
;; - should return NIL
(defun group-grouped-at1 (grouped-rows n &key (test #'equalp))
  "Given list GROUPED-ROWS (a list of lists of lists). Each sublist represents a group. Within each group, group based on applying test TEST, comparing elements at the Nth position."
  (when grouped-rows
    (let ((new-grouped-rows nil))
      ;; GROUPED-ROWS-GROUP - a list of rows, already grouped with at least one item in common - now id dups at next clabel...
	(do ((grouped-rows-group (pop grouped-rows) (pop grouped-rows)))
	    ((not grouped-rows-group))
	  ;; GROUPED-ROWS2 - newly grouped GROUPED-ROWS-GROUP
	  (let ((grouped-rows-group2
		 (remove-if #'(lambda (some-list)
				(< (length some-list) 2))
			    (funcall #'dcu:group-by2 n grouped-rows-group :test test))))
	    (if grouped-rows-group2
		(setf new-grouped-rows (append grouped-rows-group2 new-grouped-rows)))))
      new-grouped-rows)))

;;;;;;;;;;;; above should go in dat-cl-utils/shared-list

(defun ensure-*sqlgcon*-set ()
  "Set *SQLGCON* to the appropriate value. If a connection to the default database exists, then *SQLGCON* should be set to that connection object. If such a connection does not exist, then *SQLGCON* should be nil. Return value undefined."
  (if (db-connected-p)
      (setf *sqlgcon* (default-database-object))))

(defun ensure-connection ()
  "Return NIL if unable to ensure that a connection exists to the default database (see DEF-DB-PARAMS and *DB*). Otherwise return a true value."
  (if (db-connected-p)
      t
      (progn
	(warn "SQLG: ENSURE-CONNECTION: attempting reconnect...")
	(database-connect)
	(db-connected-p))))

(defun %insert-into (table field-labels values &key (db *db*))
  (let ((insert-string
          (with-output-to-string (s)
            (insert-string-a table field-labels s)
            (write-string " VALUES ( " s)
            (sqlg::data-to-sql-stream-raw values s)
            (write-string " );" s))))
    (sqlg::exec-single-sql-command insert-string db)))

(defun insert-string-a (table field-labels s)
  "S is an output stream."
  (write-string "INSERT INTO " s)
  (write-string table s)
  (write-string " ( " s)
  (sqle::lbls2 field-labels :out s)
  (write-string " ) " s))

;;(defun exec-single-sql-command  (sqlstring &optional database-object-specifier)
;;  "Execute SQL command represented by string SQLSTRING; use database specified by DATABASE-OBJECT-SPECIFIER (with postmodern, a 'database-connection' object). Return value undefined. note(s): - command is singular for a reason - SQLSTRING should contain a single SQL statement; some database backends give unpredictable results if multiple SQL statements are crammed into SQLSTRING (e.g., clsql doesn't support 'begin transaction;...; end transaction;' as SQLSTRING) Conditions: signal an error if SQL database signals an error."

(defun insert-or-replace		; insert-or-replace-row3
    (table columns row &key (db *db*))
  "Perform a SQL INSERT OR REPLACE action on table TABLE. COLUMNS is a list of strings specifying column labels. ROW is a list of data (the order corresponding to that of COLUMNLABELS) which should be updated in that row. Use database connection CONNECTION if specified. Data in ROW is transformed by SAFE-DATA before being submitted to the database.

note(s):

  - this function may not be supported by all SQL implementations (historically, postgresql has eschewed this functionality since it is not a part of the SQL standard; historically, sqlite and mysql have supported this functionality)
  - a desire to use this function might be a good indicator that it is time to take a long hard look at database and/or software design - desire for this functionality may mean things are not designed very well at one or both levels"
  (exec-single-sql-command
   (eval
    (insert-or-replace-string table columns
			      (safe-data row)))
   db))

(defun insert-or-replace-row2-TESTING
    (key keycolumn columnlabels data table)
  "Assumes KEY is true and that it is a primary key. If that key doesn't exist in the database table TABLE, add the record. Otherwise, update data in existing record in the database table specified by that key value where the key column is titled KEYLABEL where COLUMNLABELS is a list of strings specifying the 'column label' of each datum and DATA is a sexp with the corresponding data atoms."
    (exec-single-sql-command
     (eval
      (insert-or-replace-format-sexp-TESTING
       table
       ;; add...format-sexp has similar mapcar - maybe should be a defun?
       (mapcar #'(lambda (x)
		   (dcu:bracket-with-apostrophes x))
	       (cons keycolumn columnlabels))
       (cons key data) ))))

(defun insert-or-replace-format-sexp-TESTING (table columns data)
  "Assemble an EVAL'able FORMAT statement yielding a SQL statement for inserting
a record to the SQL database table TABLE where COLUMNS is a sexp with one or more strings specifying the 'column labels' and DATA is a sexp containing the corresponding data atom(s)s. ** note that this code parallels add...format sexp -- changes here should be considered for changes in that function **"
  (let ((front-string "insert or replace into ~A (")
	(columns-string "")
	(values-string ") values (")
	(end-string ");"))
    ;; FIXME: replace with a call to LABELS-STRING
    ;; assemble format columns string
    (do ((x 0 (1+ x)))
	((= x (length columns)))
      ;; if we needed to get the current column value
      ;;(let ((column (elt columns x)))
	
      (setf columns-string (concatenate 'string columns-string "~A,"))
      ;; we should be able to assume that data matches columns
      ;; otherwise SQL will likely choke anyways...
	
      ;; so, assemble format values string
      ;; - use ~S (prin1) if it's not a string
      ;;   so lisp-specific stuff is readable by lisp on the way back...
      (if (stringp (elt data x))
	  (setf values-string
		(concatenate 'string values-string "'~A',"))
	  (setf values-string
		(concatenate 'string values-string "'~S',"))))
    (setf columns-string (string-right-trim "," columns-string))
    (setf values-string (string-right-trim "," values-string))
      
    ;; assemble sexp to return
    (let ((string-component
	   (concatenate 'string
			front-string
			columns-string values-string end-string))
	  ;; use data as starting point for building sexp
	  (sexp-to-return data))
      ;; add content of columns, not list...
      (setf sexp-to-return (append columns sexp-to-return))
      (push table sexp-to-return)
      (push string-component sexp-to-return)
      (push 'nil sexp-to-return)
      (push 'format sexp-to-return)
      sexp-to-return)))

(defun insert-or-replace-string (table columns data)
  "Return string representing complete SQL INSERT OR REPLACE statement
were COLUMNS is a list of strings specifying column labels. DATA is a
list of data (the order corresponding to that of COLUMNS) which should
be updated in that row."
  ;; CSTREAM: column string stream
  (let ((cstream (make-string-output-stream)))
    (mapcar #'(lambda (c)
		(write-string (dcu:concs "'" c "', ") cstream))
	    columns)
    (let ((dstring (data-to-sql-string-raw data))
	  (cstring
	    (string-right-trim " ," (get-output-stream-string cstream))))
      (format nil "INSERT OR REPLACE INTO ~A ( ~A ) VALUES ( ~A );" table cstring dstring))))

;; * indicates parameterized version of command
(defun insert-string* (table columns)
  "Return string representing complete parameterized SQL INSERT statement where COLUMNS is a list of strings specifying column labels."
  (declare (string table)
	   (cons columns))
  (the string
       ;; cl-sqlite is sensitive to any whitespace after the semicolon when this is used as a single command
       (with-output-to-string (s)
	 (write-string "INSERT INTO " s)
	 (write-string table s)
	 (write-string " ( " s)
	 (sqle::lbls--stream columns s :sep-char #\,)
	 (write-string " ) VALUES ( " s)
	 (sqle::?s (length columns) s)
	 (write-string " );" s))))

;; test:
;; PHASS> (sqlg::labels-string (list "joe" "foo" "bob"))
(defun labels-string (some-list)
  "Return a string suitable for a list of columns in a SQL select
statement where SOME-LIST contains strings corresponding to column
labels."
  ;; FIXME: seems likely this isn't particularly efficient...
  ;; see also LIST-TO-STRING,
  (dcu:merge-list-of-strings (dat-cl-utils:commas-in-string-list some-list)))

;; FIXME: should support loading whole set of systems...
(defun load-db-type-code (&optional dbtype systems)
  "Load code for interface to database of type DBTYPE where type is :SQLITE3 or :POSTGRESQL. If DBTYPE is nil, use (db-type *db*) to determine which database type to load code for. If SYSTEMS is specified, it should be a list of ASDF-loadable system names, to be loaded instead of the default behavior."
  (init-db-type (db-type *db*))
  ;; in a transition between sqlg-FOO and sqlg-MOO
  ;; the code below will, one-by-one, hit name conflicts and query
  ;; user with respect to resolution
  (let* ((d (if dbtype dbtype (db-type *db*)))
	 ;; SYNTAXSYS: system defining implementation-specific syntax
	 (syntaxsys
	  (cond (systems nil)
		((eq d :sqlite3)
		 :sqlg-sqlite		; backend
		 )
		((eq d :postgresql)
		 ;;:sqlg-pg
		 :sqlg-pg-dot-lisp)
		(t (error "unsupported database type"))))
	 ;; BACKENDSYS is ASDF system for lisp backend to specific SQL engine
	 (backendsys
	  (cond (systems nil)
		((eq d :sqlite3)
		 :sqlg-clsql)
		((eq d :postgresql)
		 :sqlg-postmodern)
		(t
		 (error "unsupported database type")))))
     ;;(format t "LOAD-DB-TYPE-CODE: sqlsystem: ~A~%" sqlsystem)
     
    ;; this can get us into a mess during development
     ;; - if sqlg is modified but sqlg-foo is untouched, dummy variable defs get loaded, overwriting more recent versions... including overwriting INIT-DB-TYPE (which then fails directly below...)
     (if systems
	 (dolist (system systems)
	   (asdf:operate 'asdf:load-op system))
	 (progn
	   (asdf:operate 'asdf:load-op syntaxsys)
	   (asdf:operate 'asdf:load-op backendsys)))

     ;; this function is redefined in SQLG-FOO appropriately...
     ;;(format t "LOAD-DB-TYPE-CODE calling INIT-DB-TYPE ->~S<-~%" d)
     ;;(init-db-type d)
     ))

(defun nonunique? (x columnlabel table &optional (db *db*))
  "Return a true value if value X is present one time or zero times in column COLUMNLABEL in table TABLE. Otherwise (if value X is present more than one time in column COLUMNLABEL) return NIL."
  (assert (stringp columnlabel))
  (assert (stringp table))
  ;; select cnstrtype, count(cnstrtype) as numoccurrences from columndescriptions group by cnstrtype having ( count(cnstrtype) > 1 );
  ;; list of lists; each sublist has value in first position and count in second position
  (let* (;; list of form (("triangular" 3))
	 ;; each sublist represents an item occurring 2 or more times in column
	 (duplicate-values-and-counts
	  (query* (dcu:concs "select " columnlabel ", count(" columnlabel ") as numoccurrences from " table " group by " columnlabel " having ( count(" columnlabel ") > 1);"     )
		  db))
	 (duplicate-values
	  (if duplicate-values-and-counts
	      (mapcar #'(lambda (x) (car x)) duplicate-values-and-counts))))
    (find x duplicate-values :test #'equal)))

(defun number-of-rows-in-table (table &optional (db *db*))
  "Return an integer. Note: this is relatively slow with large tables
(e.g., with postgresql, execution requires a second or more once
tables get into the millions of rows)."
  (assert (stringp table))
  (the (values fixnum &optional)
       (dcu::to-integer-strict
	(caar (query* (dcu:concs "select count(*) from " table ";") db)))))

;; FIXME: rename ROW once ssqlfs/sqlg ROW overlap issues resolved...
(defun row-sqlg (table sql-expr &optional (db *db*))
  "Return list representing row from database table TABLE. Return a
list of column labels as a second value. If specified, use database
connection DB. Otherwise, use default database connection specified by
*DB*."
  (declare (string sql-expr table))
  (let* ((columnlabels (columnlabels-in-table table db))
	 ;; FIXME: use LABELS-STRING ? or stop using LABELS-STRING and only use this...
	 (columnlabels-string
	  (dcu:list-to-string columnlabels ",")))
    (assert columnlabels)		; sanity check; if table apparently lacks fields/columns, something has almost certainly gone awry
    (multiple-value-bind (rows columnlabels2)
	(query*
	 ;; only useful if all implementations return column labels...
	 ;; (dcu:concs "select * from " table " where " sql-expr ";"))
	 (dcu:concs "select " columnlabels-string " from " table " where " sql-expr ";")
	 db)
      (format t "rows: ~S~%" rows)
    (values (car rows)
	    (if columnlabels2 columnlabels2 columnlabels)))))

(defun row-as-hash (table sql-expr &key (db *db*) (test 'equal))
  (multiple-value-bind (vals field-labels)
      (row-sqlg table sql-expr db)
    ;; 'equal to facilitate using string keys
    (dcu::hash-from-lists field-labels vals :test test)))

(defun rows-from-table-chunked (table start-key stop-key keycolumn &key db)
  "Return rows from database table TABLE with primary keys in range of integers from integer START-KEY to integer STOP-KEY (inclusive) in column KEYCOLUMN specified by KEY. Return as a second value the list containing the corresponding column labels as strings.

Notes:

CHUNKED: everything enclosed in a single SQL transaction."
  (assert start-key)
  (assert stop-key)
  (assert keycolumn)
  (assert table)
  (multiple-value-bind (rows columnlabels)
      (query* (dcu:concs "select * from " table " where " keycolumn " BETWEEN " start-key " AND " stop-key ";")
	      db)
    (values rows columnlabels)))

;; FIXME: optimize -- does this cons a lot?
;; - could certainly optimize and not call COLUMNLABELS-IN-TABLE if table structures are fixed (just establish column/field labels once and store in memory)
(defun rows-where (table sql-expr &optional columnlabels db)
  "Return rows in table where the string SQL-EXPR (a SQL expression which can succeed the 'where' operator) applies.

The fields/columns, and their order, is specified by COLUMNLABELS. A cached set of field labels is used if COLUMNLABELS is NIL.

Return, as a second value, the list of column labels. See also ALL-ROWS."
  (declare (list columnlabels)
	   (string sql-expr table))
  ;; If COLUMNLABELS is NIL, use cached column labels. This is a place where caching makes sense.
  (let ((final-columnlabels (or columnlabels
				;; use cached field labels
				(columnlabels-in-table table))))
					;(log:debug final-columnlabels)
    (let ((query-string (rows-where-query-string table sql-expr final-columnlabels)))
					;(log:debug query-string)
      (multiple-value-bind (rows columnlabels?)
	  (query* query-string (or db *db*))
	(values rows
		final-columnlabels)))))

(defun rows-where-query-string (table sql-expr columnlabels)
  "Return a string. COLUMNLABELS is a list of strings."
  (format nil "select ~A from ~A where ~A;"
	  (dcu:list-to-string
	   (dcu:commas-in-string-list columnlabels))
	  table
	  sql-expr))

(defun rows-where-column-contains (table columnlabel value &optional (db *db*))
  "Return rows in table where column COLUMNLABEL contains value VALUE."
  (assert (and table columnlabel value))
  ;; facilitate returning column labels as a second value
  (let ((columnlabels (columnlabels-in-table table)))
    (multiple-value-bind (rows columnlabels?)
	(query* (rows-where-column-contains-string table columnlabel value columnlabels)
		db)
      (values rows
	      (if columnlabels?
		  columnlabels?
		  columnlabels)))))

(defun rows-where-column-contains-string (table columnlabel value columnlabels)
  (format nil "select ~A from ~A where ~A like '%~A%';"
	  (dcu:list-to-string (dcu:commas-in-string-list columnlabels))
	  table
	  columnlabel
	  value))

(defun rows-where-any-of-columns-contain (table search-columnlabels value &optional (db *db*))
  "Return rows in table where column COLUMNLABEL contains value VALUE."
  (assert (and table search-columnlabels value))
  ;; facilitate returning column labels as a second value
  (let ((columnlabels (columnlabels-in-table table)))
    (multiple-value-bind (rows columnlabels?)
	(query* (rows-where-any-of-columns-contain-string table search-columnlabels value columnlabels)
		db)
      (values rows
	      (if columnlabels? columnlabels? columnlabels)))))

(defun rows-where-any-of-columns-contain-string (table search-columnlabels value all-columnlabels)
  (with-output-to-string (s)
    (format s "select ~A from ~A where "
	  (dcu:list-to-string (dcu:commas-in-string-list all-columnlabels))
	  table)
    (sqle::lblsval search-columnlabels value s)))

(defun safe-data (data)
  "DATA is a list of strings or NIL values. Return a list of strings
(or NIL values) representing 'sql-safe' versions of the objects in
DATA. See SAFE-DATUM."
  (mapcar #'(lambda (x)
	      (cond ((numberp x)
		     x)
		    ((stringp x)
		     (safe-datum x))
		    (t nil)))
	  data))

(defun safe-datum (x)
  "Return string representing 'sql-safe' version of string X.

NEW: X must be a string - there's nothing safe about apostrophes, etc. passing through on the fly..."
  (assert (stringp x))
  (dcu:apostrophe-to-double x)
  ;; any other SQL problem chars/strings/?? besides apostrophes?
  )

(defun create-table (name columns &key (db *db*) types)
  "Create a new SQL table named NAME with column labels corresponding to the strings in list COLUMNS. TYPES is an optional list specifying the data type which each column should hold."
  (if (tablep name)
      (error "table exists")
      (let ((command (create-table-string
                      name
                      columns
                      types)))
        (exec-single-sql-command command db))))

;; avoid 'SEARCH' as SEARCH is a CL fn
(defun search-table (table search-column search-string &key query-fragment (db *db*))
  "Return, as first value, list of lists (row data). Return, as second value, corresponding list of column labels.

Search for records in database table TABLE where the value in column with label SEARCH-COLUMN. Use the SQL operator LIKE with SEARCH-STRING. Net effect: return records which contain string SEARCH-STRING as a substring. Not case-sensitive. SQL wild-cards can be used. Zero-length strings can be matched.

QUERY-FRAGMENT, if a string, will be appended verbatim to the query.

note(s): 
  - might be desirable to extend this to explicitly supporting regex searches."
  (declare (string search-column search-string table))
  (columns-where
   table
   (columnlabels-in-table table)
   (with-output-to-string (s)
     (format s "~A LIKE '%~A%' " search-column search-string)
     (if (stringp query-fragment) (write-string query-fragment s)))
   :db db))

(defun tablep (name &optional (db *db*))
  "Return a true value if database table named NAME (a string) exists. Return NIL otherwise. note(s): add owner as optional arg?"
  (member name (table-names :all-p nil :db db) :test #'string=))

(defun tables-with-columnlabel (columnlabel &optional (db *db*))
  (multiple-value-bind (clabels-by-table tables)
      (columnlabels-in-db db)
    (let ((return-tables nil))
      (do ((x 0 (1+ x)))
	  ((>= x (list-length tables)))
	(let ((clabels (nth x clabels-by-table))
	      (table (nth x tables)))
	  (if (member columnlabel clabels :test #'string=)
	      (push table return-tables))))
      return-tables)))

(defun unique? (x columnlabel table &optional (db *db*))
  "Return a true value if value X is present one time or zero times in column COLUMNLABEL in table TABLE. Otherwise (if value X is present more than one time in column COLUMNLABEL) return NIL."
  ;; select cnstrtype, count(cnstrtype) as numoccurrences from columndescriptions group by cnstrtype having ( count(cnstrtype) > 1 );
  ;; list of lists; each sublist has value in first position and count in second position
  (let* (;; list of form (("triangular" 3))
	 ;; each sublist represents an item occurring 2 or more times in column
	 (duplicate-values-and-counts
	  (query* (dcu:concs "select " columnlabel ", count(" columnlabel ") as numoccurrences from " table " group by " columnlabel " having ( count(" columnlabel ") > 1);"     )
		  db))
	 (duplicate-values
	  (if duplicate-values-and-counts
	      (mapcar #'(lambda (x) (car x)) duplicate-values-and-counts))))
    (not (find x duplicate-values :test #'equal))))

;; note: formerly UPDATE-ROW-WHERE (alternative names considered: UPDATE-WHERE or UPDATE-ROW)
;; FIXME: would an UPDATE-ATOM be worthwhile? (less consing since a single columnlabel and a single datum)
(defun %update (table columnlabels data criterion &optional (db *db*))
  "Update row in SQL table TABLE (a string) which matches CRITERION
(the string used in a SQL 'where' clause; e.g., 'clabel = a OR clabel
= b') where 'update' refers to the SQL UPDATE operation. COLUMNLABELS
is a list of strings specifying column labels. DATA is a list of data
(the order corresponding to that of COLUMNLABELS) which should be
updated in that row. Members of DATA are strings. The return value of
this function is unspecified."
  (update-row-where02 table columnlabels data criterion db))

;; note: UPDATE-ROW-WHERE01 is grossly efficient: update of single user corresponds to a separate EXEC-SINGLE-SQL-COMMAND (a separate UPDATE SQL call) for each field
(defun update-row-where01 (table columnlabels data criterion &optional (db *db*))
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   ;;(string criterion table)
	   ;(cons columns)
	   ;(list data)
	   )
  ;(log:debug 100)
  (assert (stringp criterion))
  (assert (stringp table))
  (assert (consp columnlabels))
  (assert (listp data))
  (dolist (column columnlabels)
    (let (

	  ;; FIXME: move into SQLE: (prep-datum datum)
	  
	  ;; deal with problem characters, other SQL issues
	  ;; treat NIL as NULL
	  ;; surround strings with the ' character
	  (safe-datum (let ((datum (pop data)))
			;(log:debug datum)
			(with-output-to-string (s)
			  (cond (datum
				 (write-char #\' s)
				 (write-string (dcu:apostrophe-to-double datum) s)
				 (write-char #\' s))
				(t
				 (write-string "NULL" s))))))
	  )
      ;(log:debug safe-datum)
    
      ;; FIXME: ?? one column at a time ?? - why not MAPCAR to new data and then all at once??

      (exec-single-sql-command
       (dcu:concs  "UPDATE " table " SET " column "= " safe-datum " WHERE " criterion ";")
       db))))

(defun update-row-where02 (table columnlabels data criterion &optional (db *db*))
  "Update row in SQL table TABLE (a string) which matches CRITERION (the string used in a SQL 'where' clause; e.g., 'clabel = a OR clabel = b') where 'update' refers to the SQL UPDATE operation. COLUMNLABELS is a list of strings specifying column labels. DATA is a list of data (the order corresponding to that of COLUMNLABELS) which should be updated in that row. Members of DATA are strings. The return value of this function is unspecified."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string criterion table)
	   (cons columnlabels)
	   (list data))
  (exec-single-sql-command
   (dcu:concs "UPDATE " table " SET "
	  (with-output-to-string (s) (se:lblsvals-s columnlabels data s :separator ","))
	  " WHERE " criterion ";")
   db))

(defun update* (table columnlabels data criterion &optional (db *db*))
  "DATA is a list of strings."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string criterion table)
	   (cons columnlabels)
	   (list data))
  (exec-single-sql-command*
   (update-string* table columnlabels criterion)
   db
   data))

(defun update-string* (table columnlabels criterion)
  (with-output-to-string (s)
    (write-string "UPDATE " s)
    (write-string table s)
    (write-string " SET " s)
    (se::set-list* columnlabels s)
    (write-string " WHERE " s)
    (write-string criterion s)
    (write-string ";" s)))

;; FIXME: deprecate (trigger error) and encourage use of UPDATE*
;; * denotes parameterized version
(defun update-row-where* (table columnlabels data criterion &optional (db *db*))
  "Returns nothing."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string criterion table)
	   (cons columnlabels)
	   (list data))
  (error "Use UPDATE* (parameterized)")
  (exec-single-sql-command*
   (update-row-where-statement table columnlabels criterion)
   db
   data))

(defun update-row-where-statement (table columnlabels criterion)
  (with-output-to-string (s)
    (write-string "UPDATE " s)
    (write-string table s)
    (write-string " SET " s)
    (se::set-list* columnlabels s)
    (write-string " WHERE " s)
    (write-string criterion s)
    (write-string ";" s)))

(defun update-S (table field-labels values expr)
  "EXPR is a string representing an SQL expression which defines the row which will be operated upon (the string will succeed the WHERE operator)."
  (declare (optimize (safety 3) (debug 3))
	   (string expr table)
	   (cons field-labels)
	   (list values))
  (assert (dcu::list-of-strings-p (car field-labels)))
  (if (row-sqlg table expr)
      (update-row-where* table field-labels values expr)
      (error 'nonexistent-record)))

;; FIXME: this code is (functionally) identical to that of UPDATE-ROW-WHERE except for the function name acknowledging that CRITERION may potentially refer to multiple rows -->> better to rename 'UPDATE-WHERE' ??
(defun %update-rows-where (table columns data criterion &optional (db *db*))
  "Update rows in SQL table TABLE (a string) which match CRITERION (the string used in a SQL 'where' clause; e.g., 'clabel = a OR clabel = b') where 'update' refers to the SQL UPDATE operation. COLUMNS is a list of strings specifying column labels. DATA is a list of data (the order corresponding to that of COLUMNLABELS) which should be updated in that row. Return value? Members of DATA are strings."
  (declare (optimize (speed 3) (safety 3) (debug 1)))
  (assert (stringp criterion))
  (assert (stringp table))
  (assert (consp columns))
  (assert (listp data))
  (dolist (column columns)
    (let (;; deal with problem characters, other SQL issues
	  (safe-datum (dcu:apostrophe-to-double (pop data))))
      (exec-single-sql-command
       (dcu:concs  "UPDATE " table " SET " column "='" safe-datum "' WHERE " criterion ";")
       db))))

(defun update-field (table columnlabel value criterion &optional (db *db*))
  "Update row(s) in SQL table TABLE (a string) which match CRITERION
(the string used in a SQL 'where' clause; e.g., 'clabel = a OR clabel
= b') where 'update' refers to the SQL UPDATE operation. COLUMNLABEL
is a string specifying a field. VALUE is a string. The return value of
this function is unspecified."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string columnlabel criterion table value))
  (exec-single-sql-command*
   (update-string* table
		   (list columnlabel)
		   criterion)
   db
   (list value)
   )
  ;; (exec-single-sql-command
  ;;  (dcu:concs "UPDATE " table " SET "
  ;; 	  (se:pair columnlabel value) " WHERE " criterion ";")
  ;;  db)
  )

(defun %update-field-all (table columnlabel value &optional (db *db*))
  "Update row(s) in SQL table TABLE (a string) via an SQL UPDATE operation. COLUMNLABEL is a string specifying a field. VALUE is a string. The return value of this function is unspecified."
  (declare (optimize (speed 3) (safety 3) (debug 1))
	   (string columnlabel table value))
  (exec-single-sql-command
   (dcu:concs "UPDATE " table " SET "
	      (se:pair+ columnlabel value nil nil))
   db))

(defun vacuum (&optional (db *db*))
  "VACUUM database."
  (exec-single-sql-command (format nil "VACUUM;") db))

(defun values-in-mmtable (mmtable label &key (db *db*) value2 label2)
  "Return a list of values in database table MMTABLE in column with
name LABEL. If KEY2 and KEYLABEL2 are defined, restrict the list of
values returned to those which are in rows where the value in column
KEYLABEL2 is equal to KEY2. NIL values are silently discarded from the
list returned."
  (remove
   nil
   (if (and value2 label2)
       ;; restrict based on value2
       (mapcar (lambda (x)
		 (car x))
	       (columns-where
		mmtable (list label)
		(se:lblsvals (list label2) (list value2))
		:db db
		:unique-p t))
       ;; all keys in column keylabel
       (column mmtable label :db db :unique-p t))))

  ;; note: REALLY slow for large (1e6 records) table...
(defun value-to-column (value column table &optional (db *db*))
  "Set column COLUMN to value VALUE for each record in table TABLE."
  (query* (format nil "update ~A set ~A = ~A;" table column value) db))
