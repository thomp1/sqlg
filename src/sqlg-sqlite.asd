(defsystem sqlg-sqlite
  :serial t
  :components ((:module "sqlite"
		:components
		((:file "sqlg-sqlite"))))
  :depends-on (:sqlg
	       :sqlite			; cl-sqlite
	       :dfile
	       :uiop))
