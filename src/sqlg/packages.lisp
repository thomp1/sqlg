(in-package :cl-user)

(format t "***************~%~%SQLG should be loaded prior to loading both, ~%  (1) SQLG-PG or SQLG-SQLITE to load the appropriate database-specific code~%  (2) SQLG-POSTMODERN or SQLG-CLSQL to load the desired lisp front-end for the database~%~%*******************~%")

;; Uncomment this for warnings regarding not utilizing static tables
;;(push :SQLG-STATIC-TABLE-WARNINGS *FEATURES*)

(defpackage :sqlg
  (:use :cl)
  (:export
   ;; conditions
   nonexistent-record
   ;; variables
   *db*
   ;; SQL statements
   add-or-update*
   add-or-update-mm*
   add-or-update-mm-row
   add-row*
   add-row-S
   add-row-s-pk
   add-rows
   all-rows
   atom-in-column-p
   column
   column-where
   columns
   columns-where
   create-table
   datum
   datum-where2
   delete-table
   %delete-where
   delete-where*
   exec-single-sql-command
   query*
   rows-where
   search-table
   update-field
   update*
   vacuum
   ;; the database
   check-sql
   db-connected-p
   db-host
   db-name
   db-pass
   db-type
   db-user
   def-db-params
   disconnect-sql
   dbparams
   load-db-type-code
   make-db
   ;; sql-util.lisp
   fill-column
   pprint-table
   search-replace-rows
   ;; misc
   backup
   columnlabel-in-table-p
   columnlabels-in-table
   tablep
   ;; cached table structure (static.lisp)
   declare-static
   )
  (:documentation
   "SQLG is composed of a few convenience functions for working with a SQL database. The program is released under the terms of the Lisp Lesser GNU Public License http://opensource.franz.com/preamble.html, also known as the LLGPL. Copyright: David A. Thompson, 2009-2021."
   ))
