(in-package :sqlg)
;;;
;;; vars.lisp --- variable definitions for sqlg
;;;
(defstruct db
  "The `db` structure defines the parameters necessary to connect to a
  specific database. The `type` slot specifies the database type (the
  database engine): supported values include :sqlite3 and :postgresql.

The `host` slot, for sqlite, should be the absolute path for a sqlite
database file. The `name` and `pass` slots are meaningless for sqlite.

The `user` slot specifies the user name for database. Meaningless for
sqlite. Corresponding to owner of database tables in postgresql. With
clsql, :all corresponds to all users."
  host
  name
  pass
  type
  user)

(defvar *db* nil "The default database to connect to. A db structure.")

(defparameter *sqlgcon* nil "Default connection object.")

(defvar *sqlg-lisp-front-end*
  "Keyword indicating which lisp SQL front-end was loaded most recently."
  nil					; :CL-SQLITE, ...
  )
