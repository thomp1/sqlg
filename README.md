# SQLG

Convenience functions for interacting with an SQL database.

The SQLG system describes code in the SQLG package which isn't specific to a particular database engine or database backend. 

To get things up and running with a specific SQL implementation/engine/backend, load the corresponding system. For example, for an sqlite database, load the SQLG-SQLITE system.


# Conventions for function names

The percentage character, %, at the start of a function name indicates that the command is not parameterized or is otherwise to be handled with care.

For the time being, the asterisk at the end of a command name denotes a parameterized version of the command. Perhaps once %FOO commands are all in place, parameterized command(s) named as FOO* will be renamed as FOO.


# The database specification

The `db` structure defines parameters used to connect to a specific database.


# The structure of the SQLG folder

The SQLG folder contains all system definitions for all front-ends, all engines, etc.


# Additional details on relationships between systems

## SQLG
- shared SQL stuff
- includes definitions which are rewritten by sqlg-[dbtype] and sqlg-[lisp-frontend]
- SQLG should be loaded before any other systems

## SQLG-[DBTYPE]
- where [dbtype] is pg (for postgresql) or sqlite (for sqlite3)
- defines implementations of queries/commands specific to the database type

## SQLG-[LISP-FRONTEND]
- where lisp-frontend is one of postmodern (postgresql only), clsql (for sqlite3 or postgresql), pg-dl (pg-dot-lisp; for postgresql)
- defines implementations of queries/commands specific to the frontend
