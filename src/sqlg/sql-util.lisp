(in-package :sqlg)
;;;
;;; sql-util.lisp: database utility functions
;;;
(defun average-for-column (column table)
  "Return the average value for a column COLUMN in table TABLE,
ignoring non-numeric values."
  (alexandria:mean
   (dcu:remove-nils
    (dcu:coerce-to-numbers
     (column table column)))))

(defun db-diff (db1 db2 &key verbosity)
  "Compare database DB1 and database DB2."
  (let ((tables1 (table-names :db db1))
	(tables2 (table-names :db db2)))
    (when (eq verbosity :high)
      (format *standard-output* "tables in db1: ~S~%" tables1)
      (format *standard-output* "tables in db2: ~S~%" tables2))
    (let ((unique-tables1 (set-difference tables1 tables2 :test 'string=))
	  (unique-tables2 (set-difference tables2 tables1 :test 'string=)))
      (format *standard-output* "tables unique to db1: ~S~%" unique-tables1)
      (format *standard-output* "tables unique to db2: ~S~%" unique-tables2))

    (multiple-value-bind (column-labels1 table-labels1)
	(columnlabels-in-db db1)
      (multiple-value-bind (column-labels2 table-labels2)
	  (columnlabels-in-db db2)
	(map nil #'(lambda (table-label)
		     (format *standard-output* "~%table: ~A~%------------------~%" table-label)
		     (let ((column-labels1 (elt column-labels1 (position table-label table-labels1 :test 'string=)))
			   (column-labels2 (elt column-labels2 (position table-label table-labels2 :test 'string=))))
		       (when (eq verbosity :high)
			 (format *standard-output* "columns in db1: ~S~%" column-labels1)
			 (format *standard-output* "columns in db2: ~S~%" column-labels2))
		       (format *standard-output* "columns unique to db1: ~A~%" (set-difference column-labels1 column-labels2 :test 'string=))
		       (format *standard-output* "columns unique to db2: ~S~%" (set-difference column-labels2 column-labels1 :test 'string=))))
	     (intersection tables1 tables2 :test 'string=))))))

(defun fill-column (table columnlabel value &key (db *db*) overwrite-p)
  "Return value unspecified. Fill column COLUMNLABEL in table TABLE with value VALUE. Don't overwrite records if OVERWRITE-P is nil (implementation: if a record contains anything apart from nil or an empty string at the position, it will not be overwritten)."
  (cond (overwrite-p
	 (%update-field-all table columnlabel value db))
	(t
	 ;; current implementation won't match on rows with nils
	 ;;(search-replace-rows table columnlabel "" value)
	 (multiple-value-bind (rows columnlabels)
	     (dump-table table db)
	   (let ((replace-column-index
		  (position columnlabel columnlabels :test 'string=)))
	     ;; iterate through rows and replace item
	     (dolist (row rows)
	       (let ((row-specifier (se:lblsvals columnlabels row))
		     (overwrite-flag
		      (if overwrite-p
			  t
			  ;; check if atom is empty or not
			  (dcu:noes (nth replace-column-index row)))))
		 (if overwrite-flag
		     (update-field table columnlabel value row-specifier db)))))))))

(defun generate-integer-keys (table column)
  "In column COLUMN of table TABLE, generate primary key values using ascending integers for each record in the table. note(s): consider creating a new table with the primary key column; then drop the data in"
  ;; get all records from table (rows, columnlabels)
  (multiple-value-bind (rows columnlabels)
      (dump-table table)
    (let ((cindex (position column columnlabels :test #'equalp))
	  (key 0))
      (assert (integerp cindex))
      (dolist (row rows)
	(let ((old-row
	       (copy-list row)))
	  (setf (nth cindex row) key)
	  (setf key (1+ key))
	;; update each row, adding new key
	(update*
	 table
	 columnlabels
	 ;; UPDATE-TABLE-WHERE requires all string data
	 (dcu::coerce-to-strings-nonils row)
	 (se:lblsvals columnlabels old-row)))))))

(defun max-numeric-in-column (table columnlabel)
  (let ((nums (dcu::numbers-in-list1
	       (column table columnlabel))))
    (if nums (apply #'max nums))))

(defun pprint-rows (table &optional sql-expr columnlabels)
  (let ((clabels  (if columnlabels columnlabels (columnlabels-in-table table))))
    (multiple-value-bind (rows clabels)
	(if sql-expr
	    (columns-where table clabels sql-expr)
	    (columns table clabels))
      (map nil
	   #'(lambda (row)
	       (let ((row-len (length row)))
		 (dotimes (x row-len)
		   (format t "~A: ~A // "
			   (elt clabels x)
			   (elt row x))))
	       (format t "~%=================~%"))
	   rows))))

(defun pprint-table (table)
  "Print table content in a readable fashion."
  (multiple-value-bind (data columns)
      (dump-table table)
    (pprint-table-core table data columns)))

(defun pprint-table-core (table data columns)
  "TABLE is a string. DATA is a list of lists of arbitrary lisp
objects. COLUMNS is a list of strings."
  (let* (;; assume 80 char fixed-width display
	 ;; and try and come up with reasonable maximum column widths
	 (column-label-lengths
	   (mapcar #'(lambda (clabel)
		       (length clabel))
		   columns))
	 (number-of-cols (length columns))
	 ;; assume we'll want some sort of spacer to separate columns
	 ;; for visual pleasantness...
	 (spacer " ")
	 (total-spacer-length (* number-of-cols))
	 ;; start by alloting each column an equal share of the pie
	 (tentative-max-columnlength
	   (floor
	    (/ (- (apply #'+ column-label-lengths) total-spacer-length)
	       number-of-cols)))
	 ;; FIXME: should have a give-and-take algorithm here if some
	 ;; columns don't need much space and others could use a bit more
	 ;;	(max-col-lengths ...)
	 (max-col-lengths (make-list number-of-cols
				     :initial-element tentative-max-columnlength))
	 ;; now polish column labels and data
	 (polished-data
	   (mapcar #'(lambda (row)
		       (mapcar #'(lambda (datum max-length)
				   (let ((string-datum
					   (if datum
					       (dcu:coerce-to-string datum)
					       "")))
				     (if (> (length string-datum) max-length)
					 (subseq string-datum 0 max-length)
					 (dcu:pad-string string-datum :end (- max-length (length datum))))))
			       row max-col-lengths))
		   data))
	 (polished-columnlabels
	   (mapcar #'(lambda (clabel max-length)
		       (if (> (length clabel) max-length)
			   (subseq clabel 0 max-length)
			   (dcu:pad-string clabel :end (- max-length (length clabel)))))
		   columns max-col-lengths)))
    (format t "~A~%" table)
    (dolist (clabel polished-columnlabels)
      (write-string clabel t)
      (write-string spacer t))
    (format t "~%")
    (dolist (polished-row polished-data)
      (dolist (polished-datum polished-row)
	(write-string polished-datum t)
	(write-string spacer t))
      (format t "~%"))))

(defun search-replace-rows (table search-column search-string replace-string)
  "Search and replace in table TABLE. Evaluate records by checking if
atom in column SEARCH-COLUMN contains substring SEARCH-STRING. If so,
replace the record with the corresponding record where the atom
identified as described above is replaced with REPLACE-STRING. See
also SEARCH-TABLE."
  ;; sanity checks2
  (assert (columnlabel-in-table-p search-column table))
  (assert (tablep table))
  (assert (stringp search-string))
  (assert (stringp replace-string))
  (multiple-value-bind (rows columnlabels)
      ;; ** this isn't the best - limits things since it doesn't work with a row full of nils - only dump-table accesses these rows... - these rows don't even match with LIKE and an empty string...
      (search-table table search-column search-string)
    ;; iterate through rows and replace item
    (dolist (row rows)
      ;; ROW may have problem chars (e.g. apostrophe)

      ;; FIXME: should have a generic wrapper which deals with lisp object <-> string conversion in a uniform manner (problem: different database front-ends may dump things out differently...) for all data get/put operations from db
      ;; * this functionality should be in ssqlfs
      (let ((oldrow (safe-data
		      ;; COERCE-TO-STRINGS: this is a bad idea unless NILS are treated uniformly - otherwise need to exclude them...
		      ;; NILS-TO-EMPTY-STRINGS: a bad idea unless done uniformly since NULL values are treated distinctly from empty strings
		     ;; store all numbers as simple integer (e.g., -4352) or as simple floating point (e.g., -12.453) (prohibit any other formats such as 0.3d0, complex numbers, etc.)
		     (dcu::simple-number-strings row))))
	(format t "row: ~S~%" row)
	(format t "oldrow: ~S~%" oldrow)
	;; sanity check - something wrong if can't grab row
	(assert
	 (rows-where
	  table
	  (se:lblsvals columnlabels oldrow)))
	(update*
	 table
	 (list search-column)
	 (list replace-string)
	 (se:lblsvals columnlabels oldrow))))))

(defun tables-with-field (field-label)
  "Return a list of strings corresponding to tables with field FIELD-LABEL."
  (loop for table in (table-names)
     if (member field-label
		(columnlabels-in-table table)
		:test #'string=)
     collect table))
