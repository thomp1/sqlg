(defsystem sqlg
  :serial t
  :components ((:module "sqlg"
		 :components
		 ((:file "packages")
		  (:file "conditions")
		  (:file "static")
		  (:file "vars")
		  (:file "sqlg-foo-DOCUMENTATION")
		  (:file "sql")
		  (:file "sql-util"))))
  :depends-on (:dat-cl-utils
	       :sqle))
