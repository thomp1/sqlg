;; for CL-SQLITE front-end for sqlite
;; - see http://github.com/dmitryvk/cl-sqlite
(defsystem sqlg-cl-sqlite
  :serial t
  :components ((:module "cl-sqlite"
		 :components (
			      (:file "sqlg-cl-sqlite"))))
  :depends-on (:sqlg
	       :sqlite
	       :dat-cl-utils))
